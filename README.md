## MeleeOS

MeleeOS is a project dedicated to making a custom Linux Distribution for the sole purpose of running Super Smash Bros. Melee in the most efficient and optimal way possible.

## How Do I Contribute?

We're always looking for more people who are interested, you can get in touch with us at our Discord below, or submit a PR.

## Contact

[Here](https://discord.gg/AhpRtQN) is a link to our Discord, everyone is welcome!